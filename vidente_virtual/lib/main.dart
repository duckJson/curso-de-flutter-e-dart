import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(MaterialApp(
    title: 'Vidente Virtual',
    home: VidenteVirtualPage(),
  ));
}

class VidenteVirtualPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.brown[800],
        title: Text('Vidente Virtual'),
        centerTitle: true,
      ),
      backgroundColor: Color(0Xff332B25),
      body: Vidente(),
    );
  }
}

class Vidente extends StatefulWidget {
  @override
  _VidenteState createState() => _VidenteState();
}

class _VidenteState extends State<Vidente> {
  int videnteImagen = 1;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: TextButton(
          onPressed: () {
            setState(() {
              videnteImagen = Random().nextInt(5) + 2;
              print(videnteImagen);
            });
          },
          child: Image.asset('imagens/vidente$videnteImagen.png'),
        ),
      ),
    );
  }
}
