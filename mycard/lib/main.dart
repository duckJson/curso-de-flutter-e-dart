import 'package:flutter/material.dart';

void main() => runApp(MeuApp());

class MeuApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, // Retirar o banner de debug
      title: 'Meu Card',
      home: Scaffold(
        backgroundColor: Colors.deepOrange,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 50.0,
                backgroundImage: AssetImage('images/eu.jpg'),
              ),
              Text(
                'Matheus Monteiro',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 40.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Itim'),
              ),
              Text(
                'Desenvolvedor Web | Flutter',
                style: TextStyle(
                    color: Colors.deepOrange.shade100,
                    fontFamily: 'Source Sans Pro',
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2.5),
              ),
              SizedBox(
                height: 20.0,
                width: 250.0,
                child: Divider(
                  color: Colors.deepOrange.shade100,
                ),
              ),
              Card(
                  margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                  child: ListTile(
                    leading: Icon(
                      Icons.phone,
                      size: 25.0,
                      color: Colors.deepOrange,
                    ),
                    title: Text(
                      '(91 9 8510-6950)',
                      style: TextStyle(
                          color: Colors.deepOrange,
                          fontSize: 16.0,
                          fontFamily: 'Source Sans Pro'),
                    ),
                  )),
              Card(
                  margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                  child: ListTile(
                    leading: Icon(
                      Icons.mail,
                      size: 25.0,
                      color: Colors.deepOrange,
                    ),
                    title: Text(
                      'matheus.stack.20@hotmail.com',
                      style: TextStyle(
                          color: Colors.deepOrange,
                          fontSize: 14.0,
                          fontFamily: 'Source Sans Pro'),
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
