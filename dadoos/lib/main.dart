import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  return runApp(
    MaterialApp(
      title: 'Dadoos',
      home: Scaffold(
        backgroundColor: Colors.teal.shade700,
        appBar: AppBar(
          title: Text('Dadoos'),
          centerTitle: true,
          backgroundColor: Colors.teal.shade900,
        ),
        body: Dadoos(),
      ),
    ),
  );
}

class Dadoos extends StatefulWidget {
  @override
  _DadoosState createState() => _DadoosState();
}

class _DadoosState extends State<Dadoos> {
  int dadoEsqueda = 1;
  int dadoDireita = 1;

  void mudarDados() {
    setState(() {
      dadoEsqueda = Random().nextInt(6) + 1;
      dadoDireita = Random().nextInt(6) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: [
          Expanded(
            child: TextButton(
              onPressed: () {
                setState(() {
                  mudarDados();
                });
              },
              child: Image.asset('imagens/dado$dadoEsqueda.png'),
            ),
          ),
          Expanded(
            child: TextButton(
              onPressed: () {
                setState(() {
                  mudarDados();
                });
              },
              child: Image.asset('imagens/dado$dadoDireita.png'),
            ),
          ),
        ],
      ),
    );
  }
}
