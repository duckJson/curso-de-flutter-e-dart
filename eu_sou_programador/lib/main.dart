import 'package:flutter/material.dart';

void main() {
  return runApp(
    MaterialApp(
      title: 'Eu sou Programador',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Eu Sou Programador'),
          centerTitle: true,
          backgroundColor: Colors.amber[600],
        ),
        backgroundColor: Colors.black,
        body: Center(
          child: Image(
            image: AssetImage('images/programador.png'),
            height: 200,
            width: 200,
          ),
        ),
      ),
    ),
  );
}
